$(window).scroll(function(){
  if(window.pageYOffset > 0){
    logoimgs();
  }else{
   $(window).resize();
  }

  if(window.pageYOffset > 100){
   $("#gotop, #signup").show();
  }else{
   $("#gotop, #signup").hide();
  }

  // 我要報名按鈕m版才顯示
  var w_width = $(window).width();
  if(w_width < 481){
		if(window.pageYOffset > 100){
      $("#signup_m").show();
    }else{
      $("#signup_m").hide();
    }
	}
}).scroll();

$(window).resize(function() {
  var w_width = $(window).width();
  if(w_width <981){
		logoimgs();
	}else{
		logoimgb();
	}

  // 我要報名按鈕m版才顯示
  if(w_width <481){
		if(window.pageYOffset > 100){
      $("#signup_m").show();
    }else{
      $("#signup_m").hide();
    }
	}
}).resize();

$(function(){
  animates($("#index"));

  var w_width = $(window).width()
  // if(w_width >1025){
    $("section").css({
      "min-height":$(window).height()
    });
  // }

  // 精彩報導hover動畫
  $('.cell', '.section-grid').css({'opacity': 0, 'display': 'none'});
	$('figure', '.section-grid').hover(function() {
		$(this).find(".cell").css('display', 'block').animate({opacity:1}, 350);
	$(this).find(".text").stop().animate({top: 0}, 250);
	}, function(){
		$(this).find(".cell").css('display', 'none').animate({opacity:0}, 350);
	$(this).find(".text").stop().animate({top: '20%'}, 250);
	});

  // tab
  var _showTab = 0;
  $('.abgne_tab').each(function(){
    var $tab = $(this);
    var $defaultLi = $('ul.tabs li', $tab).eq(_showTab).addClass('active');
    $($defaultLi.find('a').attr('href')).siblings().hide();

    $('ul.tabs li', $tab).click(function() {
        var $this = $(this),
            _clickTab = $this.find('a').attr('href');
        $this.addClass('active').siblings('.active').removeClass('active');
        $(_clickTab).stop(false, true).fadeIn().siblings().hide();
        return false;
    }).find('a').focus(function(){
        this.blur();
    });
  });

  // anchor動畫
  $('.anchor_active').anchor_active({
    DynamicMenu: true,
    DynamicSection:true,
    activeClass:"active",
    KeepTopHeight: 0,
    activeCircle:false,
  });

  // lightbox
  $(".box").fancybox({
    inline:!0, 
    width:"94%", 
    padding: 0,
    beforeLoad: function () {
      $.extend(this, {
          padding: [60, 30, 80, 30]
      });
    }
  });
  $(".inline").fancybox({inline:!0, width:"94%"});
  
  $(".menu[data-box=inline]").click(function(){
    console.log($(this))
    
  })
  

  // top按鈕
	$("#gotop").click(function(){
		$("html,body").animate({scrollTop:0},500);
		return false;
	});

  // menu link動畫
  $("#smenu").click(function() {
    $("html, body").toggleClass("active");
    return false;
  });

  $('a.menu[data-box!=inline]').on("click",(function(){
    console.log($(this))
		$("#smenu").trigger("click");
	}));

  // 首頁滾軸 link
  $('a.tolink').on("click",(function(){
    var golink = $(this).attr("href");
    $("html,body").animate({scrollTop:$(golink).offset().top},800);
    return false;
	}));

   // 首頁滾軸icon動畫
   iconshake();
   function iconshake(){
    $(".iconshake").stop().animate({top:5},300, function(){
        $(".iconshake").stop().animate({top:-5},300);
    });               
   }
   setInterval(iconshake,600);

   
  //  more動畫
  // $('.mores.clp_xmoc').clp_xmoc({
  //   // ConHide : false,
  //   // Group:"headall",
  //   effects:"Slide",
  //   BodyClickToHide : false
  // });
})

function autoimgh(){
  var padding = -30;
  var autoimg0_h = $("#autoimg li").eq(0).find("img").height();
  var autoimg3_h = $("#autoimg li").eq(3).find("div").height();
  var autoimg_h = autoimg0_h + (autoimg3_h+padding);
  $("#autoimg").height(autoimg_h+autoimg3_h);

}

var speed = 1000;
var wait = 1000;

function animates(shownode){
  var _fades = shownode.find(".animate");
  var _fade_len = _fades.length;
  var total_wait = 0;
  for (var i = 0; i < _fade_len; i++) {
    (function(i){
      var thiswait = _fades.eq(i).attr("data-time") == undefined ? wait : parseInt(_fades.eq(i).attr("data-time"));
      total_wait = total_wait + thiswait;
      window.setTimeout(
      function(){
        _fades.eq(i).addClass("show");
      },total_wait);
    })(i);
  }
}

// function logoimgb(){
//   $("#header").removeClass("down");
//   $(".logo-pc a img").eq(1).fadeIn(100);
//   $(".logo-pc a img").eq(0).fadeOut(800);
// }

// function logoimgs(){
//   $("#header").addClass("down");
//   $(".logo-pc a img").eq(0).fadeOut(100);
//   $(".logo-pc a img").eq(1).fadeIn(800);
// }

function logoimgb(){
	$("#header").removeClass("down");
	$(".logo-pc a img").eq(1).fadeOut(100);
	$(".logo-pc a img").eq(0).fadeIn(800);
}

function logoimgs(){
	$("#header").addClass("down");
	$(".logo-pc a img").eq(0).fadeOut(100);
	$(".logo-pc a img").eq(1).fadeIn(800);
}