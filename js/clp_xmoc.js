(function($) {
    'use strict';
    var ModuleName = 'clp_xmoc';
    var DefaltGroupName = '';

    var Module = function(element, options) {
        this.ele = element;
        this.$ele = $(element);
        this.option = options;
    };

    Module.DEFAULTS = {
        ConHide : true,
        Group : DefaltGroupName,
        BodyClickToHide : true,
        effects : "Slide",
        TagToggleClass:false,
        PointWidth: 1025,
        callback:function(){
        }

    };

    Module.prototype.init = function(){
        var $this = this.$ele,
            $clp_allcon = $(".clp_con"),
            $clp_btn = $this.find(".clp_btn").eq(0),
            $clp_con = $this.find(".clp_con").eq(0),
            newOptions = this.option;

        if(newOptions.TagToggleClass !== false){
            var tags =Object.keys(newOptions.TagToggleClass);
            // console.log(tags)
            var tag_len = Object.keys(newOptions.TagToggleClass).length;

        }
        // console.log( $clp_btn.attr("data-ocpc")== undefined,$clp_btn.attr("data-ocm")== null);

        var PCisbind = $clp_btn.attr("data-ocpc") == null ? "true" : $clp_btn.attr("data-ocpc");
        var Misbind = $clp_btn.attr("data-ocm")  == null ? "true" : $clp_btn.attr("data-ocm");;
        var now_width = $(window).width();
        // console.log(now_width,PCisbind,Misbind);
        if ( (now_width > newOptions.PointWidth && PCisbind=="true") || (now_width < newOptions.PointWidth && Misbind=="true") ){

            if(newOptions.ConHide){
                $clp_con.hide();
                // $clp_con.addClass("Abc")
            }else{
                $this.addClass("show")
                $clp_con.addClass("show");
                if(newOptions.TagToggleClass !== false){
                    for (var i = 0; i < tag_len; i++) {
                        var key = tags[i];
                        $this.parents(newOptions.TagToggleClass[key].ParentsTag).addClass(newOptions.TagToggleClass[key].ClassName);
                    }
                }
            };

            $clp_con.data("Ggroup", newOptions.Group);
            $clp_con.data("BodyClickToHide", newOptions.BodyClickToHide);
            var hasToggleClass = newOptions.TagToggleClass == false ? false : true ;
            $clp_con.data("ToggleClass", hasToggleClass);
            if(hasToggleClass){
                $clp_con.data("ToggleClassCon", newOptions.TagToggleClass);
            }
        }
        $clp_btn.click(btnEvent);
        // var btnEvent = function() {
        function btnEvent(){
            var PCisbind = $clp_btn.attr("data-ocpc") == null ? "true" : $clp_btn.attr("data-ocpc");
            var Misbind = $clp_btn.attr("data-ocm")  == null ? "true" : $clp_btn.attr("data-ocm");
            var now_width = $(window).width();
            // console.log(now_width,PCisbind,Misbind, );
            if ( (now_width > newOptions.PointWidth && PCisbind=="true") || (now_width < newOptions.PointWidth && Misbind=="true") ){
                if(!$(this).siblings($clp_con).hasClass("show")){
                    var nowgroup =$clp_con.data("Ggroup");
                    var allremoveCalss = false;
                    var tgClass_arr = "";
                    var tgClass_index;
                    $clp_allcon.each(function(){
                        if($(this).data("Ggroup") == nowgroup && $(this).data("Ggroup") !=""){
                            $(this).removeClass("show");
                            $(this).parent(".clp_xmoc").removeClass("show");
                            if($(this).data("ToggleClass")){
                                allremoveCalss = true;
                                tgClass_arr = $(this).data("ToggleClassCon");
                                tgClass_index =$this.index();
                            }
                            goeffects ($(this));
                        }
                    })
                    if(allremoveCalss){
                        var other_tags =Object.keys(tgClass_arr);
                        var other_tags_len = Object.keys(tgClass_arr).length;
                        for (var i = 0; i < other_tags_len; i++) {
                            var other_tags =Object.keys(tgClass_arr);
                            var key = other_tags[i];
                            $clp_allcon.eq(tgClass_index).parents(tgClass_arr[key].ParentsTag).removeClass(tgClass_arr[key].ClassName);
                        }
                    }
                }
                $(this).siblings($clp_con).toggleClass("show");
                $this.toggleClass("show");
                if(newOptions.TagToggleClass !== false){
                    for (var i = 0; i < tag_len; i++) {
                        var key = tags[i];
                        $this.parents(newOptions.TagToggleClass[key].ParentsTag).toggleClass(newOptions.TagToggleClass[key].ClassName);
                    }
                }
                goeffects ($(this).siblings($clp_con));
                newOptions.callback(ModuleName);
                return false;
                
            }
            
        };

        $('body').on('click', $this, function(e) {
            var self = $this;
            var selfclp =$(e.target).parents(".clp_con").eq(0).hasClass('show');
            if (!selfclp) {
                $clp_allcon.each(function(index,ele){
                    if($(this).data("BodyClickToHide")){
                        var PCisbind = $clp_btn.attr("data-ocpc") == null ? "true" : $clp_btn.attr("data-ocpc");
                        var Misbind = $clp_btn.attr("data-ocm")  == null ? "true" : $clp_btn.attr("data-ocm");
                        var now_width = $(window).width();
                        if ( (now_width > newOptions.PointWidth && PCisbind=="true") || (now_width < newOptions.PointWidth && Misbind=="true") ){
                            $(this).parent(".clp_xmoc.show").find(".clp_btn").trigger("click");
                        }
                    }
                })
            }
        })

        var goeffects = function(action){
            var show = action.parent(".clp_xmoc").hasClass("show");
            if(newOptions.effects == 'show'){
                if(show){
                    action.show();
                }else{
                    action.hide();
                }
            }
            else{ //Slide
                if(show){
                    action.slideDown();
                }else{
                    action.slideUp();
                }
            }
        }
    };


    $.fn[ModuleName] = function(options) {
        return this.each(function() {
            var $this = $(this);
            var module = $this.data(ModuleName);
            var opts = null;
            if (!!module) {
                if (typeof options === 'string') {
                    module[options]();
                } else {
                    throw 'unsupported options!';
                }
            } else {
                opts = $.extend({}, Module.DEFAULTS, (typeof options === 'object' && options));
                var module = new Module(this, opts);
                module.init();
                $this.data(ModuleName, module);
            }
        });
    };

})(jQuery);
